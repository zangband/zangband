message("Processing src/CMakeList.txt")

add_subdirectory(lua)

set (comp_lua_files field misc monst object player random ui spell)

set (zutil_src z-util.c z-virt.c z-form.c z-rand.c z-term.c)

set (borg_src zborg1.c zborg2.c zborg3.c zborg4.c zborg5.c zborg6.c zborg7.c zborg8.c zborg9.c zbmagic1.c zbmagic2.c zbmagic3.c)

set(angband_src variable.c tables.c util.c cave.c
	object1.c object2.c monster1.c monster2.c
	xtra1.c xtra2.c spells1.c spells2.c
	melee1.c melee2.c save.c files.c fields.c
	cmd1.c cmd2.c cmd3.c cmd4.c cmd5.c cmd6.c
	store.c birth.c load.c ui.c
	wizard1.c wizard2.c grid.c streams.c rooms.c
	generate.c dungeon.c init1.c init2.c
	effects.c quest.c racial.c script.c
	artifact.c mutation.c flavor.c spells3.c
	mspells1.c mspells2.c scores.c mind.c maid-grf.c
	bldg.c obj_kind.c wild1.c wild2.c wild3.c avatar.c notes.c
	run.c
	main-win.c readdib.c angband.rc)

set(lua_gen_src)
foreach(luasrc ${comp_lua_files})
set(tolua_generated_c ${CMAKE_CURRENT_SOURCE_DIR}/l-${luasrc}.c)
add_custom_command(OUTPUT ${tolua_generated_c}
	COMMAND tolua -n ${luasrc} -o ${tolua_generated_c} ../l-${luasrc}.pkg
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lua
	DEPENDS l-${luasrc}.pkg
)
list(APPEND lua_gen_src ${tolua_generated_c})
endforeach(luasrc)

add_executable(zangband WIN32 ${lua_gen_src} ${angband_src} ${zutil_src} ${borg_src})
target_link_libraries(zangband PRIVATE lua)

if(EMSCRIPTEN)
else()
find_package(sdl2 CONFIG REQUIRED)
find_package(sdl2-image CONFIG REQUIRED)
endif()

if(EMSCRIPTEN)
SET(CMAKE_EXECUTABLE_SUFFIX ".html")
target_compile_options(zangband BEFORE PUBLIC @${PROJECT_SOURCE_DIR}/emccargs.txt)
target_link_options(zangband BEFORE PUBLIC @${PROJECT_SOURCE_DIR}/emccargs.txt)

set(RELEASE_OPTIONS -DNDEBUG -O3)
set(RELEASE_LINK_OPTIONS -O3 --llvm-lto 1)

target_compile_options(zangband PUBLIC "$<$<CONFIG:RELEASE>:${RELEASE_OPTIONS}>")
target_link_options(zangband PUBLIC "$<$<CONFIG:RELEASE>:${RELEASE_LINK_OPTIONS}>")
else()
target_compile_options(zangband PUBLIC -D_CRT_SECURE_NO_WARNINGS)
target_link_libraries(zangband PRIVATE Winmm)
#target_link_libraries(zangband PRIVATE SDL2::SDL2 SDL2::SDL2main SDL2::SDL2_image)
endif()

target_compile_features(zangband PUBLIC cxx_std_17)
set_target_properties(zangband PROPERTIES CXX_EXTENSIONS OFF)
